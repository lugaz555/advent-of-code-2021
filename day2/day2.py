def read_instructions(file):
    instructions = []
    with open(file, 'r') as f:
        for line in f.readlines():
            word, number = line.split()
            instructions.append([word, int(number)])
    return instructions


def task1(instructions):
    position_x = 0
    position_y = 0
    for instruction in instructions:
        if instruction[0] == 'forward':
            position_x += instruction[1]
        elif instruction[0] == 'down':
            position_y += instruction[1]
        elif instruction[0] == 'up':
            position_y -= instruction[1]
    return position_x * position_y


def task2(instructions):
    position_x = 0
    position_y = 0
    aim = 0
    for instruction in instructions:
        if instruction[0] == 'forward':
            position_x += instruction[1]
            position_y += aim * instruction[1]
        elif instruction[0] == 'down':
            aim += instruction[1]
        elif instruction[0] == 'up':
            aim -= instruction[1]
    return position_x * position_y


inst = read_instructions('day2.txt')
print(task1(inst))
print(task2(inst))
