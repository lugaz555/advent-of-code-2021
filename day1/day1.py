def read_file(file):
    with open(file, 'r') as f:
        numbers = []
        for line in f:
            numbers.append(int(line))
    return numbers


def task1(numbers):
    counter = 0
    for x in range(1, len(numbers)):
        if numbers[x] > numbers[x-1]:
            counter += 1
    return counter


def task2(numbers):
    counter = 0
    for x in range(3, len(numbers)):
        if numbers[x] + numbers[x - 1] + numbers[x - 2] > numbers[x-1] + numbers[x-2] + numbers[x-3]:
            counter += 1
    return counter


def task2_second_approach(numbers):
    result_list = []
    for x in range(2, len(numbers)):
        result_list.append(numbers[x] + numbers[x-1] + numbers[x-2])
    return task1(result_list)


num = read_file('day1.txt')
print(task1(num))
print(task2(num))
print(task2_second_approach(num))
